---
title: "England Day 0: Flying to Heathrow"
date: 2022-08-08 23:00:00
tagged: ["travel", "london2022"]
---

![Plane landing outside of JFK](/images/england2022/plane.png)
<sub class="caption">A plane that landed outside our gate. Shot on iPhone 13.
</sub>


`JFK, GATE A6` –– Our plane just landed at JFK, and we will be boarding soon. So
far, today has been fun! We met Aarush and Aatman (my cousins), and we are now all
chilling at the gate. Aarush is writing too, to imitate me. Mumma, for a good reason,
banned me from coding for the entire trip. Instead, I am blogging, and I intend 
to document each day.

So far, I have taken amazing pictures of the planes landing here, the cover picture
being the best. The best moment was easily when the plane (the same one as the cover photo) landed, 
pulling right next to our window. We were taking pictures, and the pilot even waved!

In any case, I really can’t wait for the trip. I won’t get much sleep today, due
to time-zone differences<sup><code>[1]</code></sup>, but nonetheless, I intend 
to fully enjoy Oxford. I cannot wait to meet Kavya there, as she has already landed.

In the end, this trip is less for a wedding (sorry bride & groom) or for 
exploration (even though that’s all we are doing). At its heart, it is a giant 
family reunion between three sisters. It will be great. 

But yes, I still will annoy people with useless history and a fake British 
accent. Otherwise, the trip will be boring for everyone!

<sub style="font-family: Georgia, Times, serif; color: grey"><code>[1]</code> On
a side note, British time is confusing. No daylight savings can be weird. 
Greenwich is _in England_, yet the time is GMT+1. Other times in the year, it is 
GMT.</sub>