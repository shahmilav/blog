---
title: "England Day 1: Oxford"
date: 2022-08-11 23:00:00
tagged: ["travel", "london2022"]
---

Yesterday after landing at LHR, we visited Oxford. It was a charming 
city with jaw-dropping architecture and fantastic food. We explored so much of 
the town that I was so tired at night that I couldn't even blog.

## Breakfast by the Radcliffe Camera
![Radcliffe camera](/images/england2022/radcliffe.png)

Our driver, Peter, dropped us off in front of the Ashmolean museum. From there,
Mumma led us to a café for breakfast. 

The café was remarkably right next to the Radcliffe camera. I had never
heard of it, but it was _the_ mark of Oxford University. Almost every picture
of Oxford includes this landmark. The stonework, architecture, and detail were
astounding.

The café, Vaults&Gardens, was beautiful, situated right underneath a church.
The food there – while expensive – tasted pretty good. I had avocado toast
along with a half cup of coffee. While the others chose to sit inside, I sat in
the beautiful weather, looking at Radcliffe and the Church.

## Christ Church College and the Bodleian Library
![Christ Church College](/images/england2022/christ-church-college.png)

After eating at Vaults&Gardens, we walked towards Christ Church College. We went
through numerous alleyways and paths. The walk was pleasant and fun. I took many
pictures. We finally reached the meadow outside the college. However, the grass 
was parched – understandably too, as Europe (and the US too) has been going 
through a significant heatwave lately.

Christ Church College, as well as being the most famous college in Oxford (well,
tied with Trinity, which was closed) is the same place where the Great Hall is in 
the Harry Potter movies. Due to this, we wanted to go. However because of 
expensive tickets and odd timings<code><sup>[1]</sup></code> it ended up being a
bust. 

After leaving Christ Church College, we decided to go to the next attraction on 
the list, Bodleian Library. The library (conveniently located _right next to 
Radcliffe Camera_) is a working library with writings on medicine, 
literature, and history. It is said to be beautiful, but unfortunately, we didn't go
since all the free tickets were sold out (which sounds stupid).

### The Ashmolean Museum

After two failed attractions, we wanted to _do_ something. By this time, Aatman
was super tired and sleepy; we had been up for **over 24 hours**. I was also 
tired but was pushing myself.

The Ashmolean is an art and archeology museum – the art was good, but the 
artifacts were FANTASTIC. Roman statues, Egyptian mummies and artifacts, and a bunch 
of Chinese and Indian things we just glanced by. At a point, Day, Riana, and I 
deviated from the rest of the group. We went to the "water closet"<code><sup>[2]
</sup></code>, drank Coke<code><sup>[3]</sup></code> and orange juice. Overall 
the museum was super fun.

### The Natural History Museum
![The excellent car coffee shop](/images/england2022/car-cafe.png)

After returning to the hotel for an hour (that nap was sorely needed), we went 
to Oxford's Natural History Museum. The first thing we see when we enter - is the gift
shop! Riana looks at a bunch of soft toys, Aatman and Aarush run to the stones, 
Kavya to the jewelry. I didn't like anything.

After the shop, we went out into the museum. There were so many dinosaur bones and
taxidermied animals. The animals were super soft, but I felt bad for them. 

In the upstairs, there was an entire hallway of birds, another for gemstones, 
and a third for bugs. The first two were pretty cool, the last, not so much.

Outside the museum, there was this nice coffee shop with great vegan 
options. We had a flat white and a flapjack.

### Meeting Mumma's friend

After running through the museum, we had planned to meet Kunal, Mumma's first 
friend since preschool. We met him at Westgate shopping center, where he came 
with his twelve-year-old son and wife.

Kunal is very nice; he bought us cupcakes and truffles and did a lot for us.
After talking with them for about thirty minutes, we went to a nice Indian 
restaurant for dinner.

Kunal's son, Krish, is a pretty cool person. At the restaurant, to Riana and Kavya's 
annoyance, Krish and I discussed English accents (his British is perfect) as well
as British politics (opinions on Brexit?)

Later, since Peter had already left, everyone else took a taxi while Kunal drove Riana and me home.

<sub style="font-family: Georgia, Times, serif; color: grey"><code>[1]</code>Why 
does everything close at five, and have a two-hour lunch break in between?</sub>

<sub style="font-family: Georgia, Times, serif; color: grey"><code>[2]</code>
Europe really couldn't think of a better name? Like seriously, "water closet"?</sub>

<sub style="font-family: Georgia, Times, serif; color: grey"><code>[3]</code> 
British coke sucks. So mild.</sub>
