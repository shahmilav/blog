---
title: "England Day 4: Blenheim Palace, part 2"
date: 2022-08-15 23:00:00
tagged: ["travel", "london2022"]
---

After leaving Leicester, we headed to Blenheim Palace for the second time. We still had a lot to do there, such as the train, the maze, and the Great Lake.

## The Walled Gardens

The first thing we did was take the train ride to the Walled Gardens. The ride 
(£1 per person) was a short, ten-minute ride from the main palace towards another area.

The first thing we did when we entered the gardens was the Butterfly Garden. It was hot and humid, but there were so many butterflies! Under each leaf, fluttering around each hibiscus flower, everywhere! There was also a small fish pond filled with bright red fish. It was beautiful. Unfortunately, none landed on me, but Kavya got a beautiful shot of one on Mumma.

After the garden, we wandered around. We saw all the old gardening equipment. There were so many things I could not tell how people knew what to use when. A two-handed saw, mechanical sprinkler, a unique blade just for oak trees. It was complicated stuff.

Then, after the shed was filled with gardening equipment, we went towards the Adventure Park. Towards the center of the park, on our left, was a giant maze. This was the same maze Mumma had hyped up two days ago, but we never got a chance to do.

The competition was on: three families, each putting in £1 each. The winner earns it all.

We all entered the maze together at first but quickly separated. In the maze's center, there were two large viewing podiums to help you find your way. I promptly reached one, but the podium did not help. All it told me was that I had to get to the middle, where the tall statue was, to find the exit. Everyone else also figured this out as well.

While my team was my family, I quickly diverted from the rest of my group. My reasoning was twofold.
 
1. Only one had to win to earn the prize money
2. I wanted to do this myself, alone.

Eventually, I reached the second side of the maze. Then began a bunch of running in circles until I found Aarush. I gave him a hint (go towards the statue) and ran away.

After that, I reached the second podium, so I had to find a way to the maze's center. I couldn't find the path from the platform, so I waved to Mumma and Day (who were on the first podium) and ran away.

Eventually, I met Aarush again. I was running left, and he was running right. Looking at me, he said, "that's a dead end," and ran off. I decided to go check out the area by myself.

It turns out Aarush was guessing. I ran down a large path under the second podium, around a large hedge wall, to the middle. Waving at Mumma and Day (still on the first podium), ran out. I ended up being the first one out, Aarush second, and Kavya third. Chaulamasi was the fourth one out.

Everyone else took _so_ long in the maze that Aarush and I decided to go again. This time, we got very lost. It took about twenty minutes to get out again; by then, we were exhausted. 

We then went to the nearby pizzeria. We had Margherita pizza as well as _lots_ of ice. Britain has a severe problem with [ice, water, and air conditioning](/posts/england/five-things-that-suck-about-england), so we were thrilled when we got cups full of ice. Everyone ate some ice and put it on their faces; I even put one in my pants!

After eating the pizza, we began the walk to the main palace. We all decided to walk instead of paying £10 again for the train. It was much more scenic than the train. I took so many lovely pictures.

Once we returned to the palace, we went to the gift shop. At first, it seemed meh, but it turned out to be good. Riana found something incredible - a brand new JellyCat Bashful Sheep, just like her favorite soft toy, Cotton. It is soft, how Cotton used to be before Riana played with it too much.

There was also an excellent book that all three families bought; I got interested in a compass, pocket watch, and British coins. In the end, I bought a beautiful magnet of a British Supermarine Spitfire.

## The Great Lake

After the shop, we walked on the bridge across the Great Lake. There were beautiful views of the back of the palace, but unfortunately, one could not capture the true beauty in a picture. 

After crossing the bridge, we approached the Harry Potter tree. At first, we assumed it would be the Whomping Willow from the _Prizoner of Azkaban_, but it was not. Instead, it was the one from the _Order of the Pheonix_. 

The tree was nice, but the view was even better. The large lake was in front of us, the trees were in the background, and the orangeish stones of the Palace were visible in the distance.

Later, we left the tree to the Palace. There was still a two-hour drive towards the City of Bath, tomorrow's attraction. 

