---
title: "England Day 5: Bath & Stonehenge"
date: 2022-08-15 23:00:00
tagged: ["travel", "london2022"]
---

Two days ago, we went to the City of Bath and Stonehenge. It was a fun day, but we partly felt that our money went down the drain (haha).

## The City of Bath
First, we left Bristol, where our hotel was, to Bath. The primary attraction in Bath was the Roman Baths (how obvious) built by the Romans in 67AD. It is one of the only natural hot spring baths in England. 

The tickets were costly. We entered, picked up audio guides (note: they suck), and looked at the Great Bath. 

<blockquote class="blockquote text-center">
It was at this moment that we knew – this was a waste of money.
</blockquote>

The "Great Bath" was a rectangular pool	, colored in a dirtier green than the River Thames, with no one even allowed to touch it (not that you would want to). The actual pool was the only Roman part, with the statues and terrace built afterward in the 1800s. Day and I looked at each other and had the same thought. People must come here, not for the bath, but for the view of Bath Abbey (again, Europe is very creative) behind us.

We finally found a way downstairs and looked through the museum. This part was more interesting. We saw a bunch of Roman coins, altars with Latin engravings, and statutes. I finally got to show my Latin knowledge to Mumma and Day. I got most things correct, including translating "Aquae Sulis" or "the waters of Sulis." Inside the museum, we saw the Great Drain (where our money went) and Roman sewage systems. 

We wandered around and saw the Head of Minerva from the temple, more coins, and a bunch of busts of people no one cares about. I was very interested in the Baths at first since I love _everything_ Roman, but this was a significant disappointment.

We then left the Roman Baths and walked to the Bath Abbey next door. There was a long line, but the abbey looked cool outside and was free.

When we entered, the lady at the front desk gave Day the whole rundown:
<figure class="text-center">
<blockquote class="blockquote text-center">
Hello, welcome to Bath Abbey. We highly appreciate donations. The recommended amounts are £12 for families, £5 for one person, and £2.5 pounds for students. How much would you like to donate?
</blockquote>
  	<figcaption class="blockquote-footer">
  	Lady from Bath Abbey
  	</figcaption>
</figure>

Day, cornered and with no choice, "donated" £12.

The abbey was boring. It was a church like any other, in no way unique. The only thing I found interesting was the fact that Edgar, the first king of England, was coronated there. 

## Stonehenge
After leaving Bath, we set our sights east, towards Stonehenge. It was a long drive, and I slept through most of it.

We arrived at the Stonehenge bus stop; from there, they take you on buses to Stonehenge. We hopped on the buses and enjoyed a ten-minute ride to Stonehenge. I was still pretty sleepy from the minibus where my nap was cut short. 

At first, Stonehenge was hidden beneath a large hill. To our left was a large stretch of open land with two Neolithic burial mounds in the center. In front of us was a bunch of tall grass. As we walked closer along the path, we saw the Sarsen stone peak out from under the hill. 

We approached Stonehenge and looked at the ancient monument. Built in about 3000 BC, without the help of modern technology such as the wheel, it was a sight to behold. Sarsen stones that weigh tons, brought from 20 miles south from the site, were lifted and arranged wonderfully. Perfectly marking the Summer and Winter solstices, it was magnificent. 

The area simply had an ancient feel. Day felt it, I felt it, and I'm sure everyone else did. It was remarkable.

People in the area were pretty weird, too - one old couple mentioned that aliens built this to suck energy from the Earth. Another was meditating, someone else praying as if this was still a religious shrine as it was 5000 years ago.

After about an hour at the site, we took the bus back to the station, and drove to Windsor. We had a two hour drive to Crowne Plaza, and the next day was long, since we had to do Windsor Castle and Buckingham Palace.

The adults had drinks at the bar at the hotel, so all of us kids had lots of fun until about 2am. No one wanted to sleep.
