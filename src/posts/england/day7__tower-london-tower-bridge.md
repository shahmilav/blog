---
title: "England Day 7: The Tower of London, Tower Bridge, and more"
date: 2022-08-25 23:00:00
tagged: ["travel", "london2022"]
---

On day seven of our trip to England, we visited attractions north of the Thames River and the Tower of London, which was a five-minute walk from our hotel. 

After waking up, we quickly went for breakfast at DeVine. DeVine is a small cafe, located near our hotel. It was okay, nothing extraordinary, but not bad either. The bagels sucked though. 

## The Tower of London

After eating at DeVine, we walked toward the Tower of London. The first thing we wanted to do at the tower was to see the Crown Jewels. They were remarkable.

We entered the tower and got free tickets due to the London Pass. As we entered, the first thing we saw was the White Tower. Since I was told to stop with my history then, I will say it now.

The White Tower is the first part of the Tower of London, built in 1066 by William the Conqueror as a sign of Norman power, and to defend London. Strategically placed along the River Thames, the Tower defined London's skyline for centuries. Over the years, it was used as a fortress, barracks, armory, and prison. Now, it is a museum.

### The Crown Jewels

We just saw the Tower from the outside and wandered around a bit inside the walls of the fort. Then, after we had regrouped, we walked towards the back side of the fortress, where the Crown Jewels were kept.

There was a thirty-minute line to the jewels, but it didn't feel that long. Once we got inside, we were able to read about the brutal history of the gems. It is a horrifying and bloody history.

Then we saw it. Golden maces, clubs, and swords all dating to the 16th century. It was beautiful. But even then, I knew that it was nothing. The collection's crown jewel (pun intended) was still to be seen. 

After the maces and clubs, we saw the coronation spoon. Dating to the 14th century, it was the oldest item in the collection and survived the abolition of the British monarchy in the 17th century, unlike many of the other jewels from then.

We walked past more swords and jewels and arrived at the section that everyone waits for: the actual crowns.

There were two conveyor belts on either side, with the jewels in the middle. This was in order to force you to keep moving.

The first diamond I saw was the Cullinan I, part of the Cullinan diamond, the largest diamond ever found.

We saw crowns, scepters, orbs, and more crowns. There were a bunch of Scottish and Welsh crowns, and ivory scepters from Queen Victoria's reign.

We saw the Imperial State Crown, fitted with the Cullinan II, a bunch of sapphires, and the Black Prince's Ruby.

Lastly came the final one. The crown of Queen Elizabeth The Queen Mother. It was the most beautiful one. At its center lay the most beautiful diamond I have ever seen – the Koh-i-Noor.

It was huge. I had to go on the conveyor belt again to properly gauge the jewels' value and beauty.

Later, we learned that the Koh-i-Noor had been surrendered by eleven-year-old Maharaja Duleep Singh and his Punjabi Kingdom to Queen Victoria.

### The White Tower

By this time, it was raining. We quickly visited a small museum that showed off British military success, and then went towards the White Tower. We also met Sonalkaki here as well.

There was a line to go up the tower, but it wasn't that bad, especially since the rain slowed to a drizzle. 

The Line of Kings was the first thing we saw when we entered the Tower. It was basically wooden horses and suits of armor from past English Kings.

We saw suits of armor in all sizes, some that could fit me and some that were at least seven feet tall. It was amazing the vast variety of heights and body structures of the kings.

We then climbed up more stairs and saw the rest of the fort. We saw a huge Norman fireplace, swords, and stairs. The stairs were scary sometimes, they were steep, narrow, and round, so you really had to pay attention.

After we finished the White Tower, there was really nothing left to do. We left the tower and walked for two minutes along the river to the Tower Bridge.

## Tower Bridge

The next step in the plan was to cross the River Thames via the Tower Bridge. The bridge really was beautiful, and I understood why it is one of the major icons of London.

I got really beautiful shots of the bridge, but one I really wanted was one of the bridge with the classic red double-decker bus crossing. I had the perfect shot lined up... and Mumma butted in, asking me to take a picture of her. All that was left of the shot was Mumma's face, covering the bus.

I took the picture of Mumma, but I was pretty upset. The perfect shot, ruined. Thankfully, I got it on the other side of the bridge, and it came out really good.

## Borough Market

After crossing the Bridge and taking at least twenty pictures, we walked further along the river to Borough Market. We got ice cream at the by the bridge, and also met Nick, Radhika, and Alex by HMS Belfast.

We then walked towards the market, but we got separated midway. Day, Lajja, Aarush, Aatman and I were in one group, the one that fell behind the others. Somehow, we ended up at Borough Market first. Turns out, there was nothing there. Day and I decided to go look for food while the other group got here, but we found nothing! We just bought very expensive cashews (£10!), and the Mexican place we were looking for was closed until 5pm.

We ended up finding nothing there, so we decided to go look for a Pizza Express nearby, and eat there.

## Along the River Thames.

We walked some more along the river to the Pizza Express. However, the first one we found was short staffed, and could not accept a party of ten people.

The next Pizza Express we found was at a much better location, with a great view of St. Pauls Cathedral across the river (which closed at 4, so we didn't get to go).

We had really good pizza here, as well as jalapeño cheese balls that were super spicy but tasted really good.

After eating at Pizza Express, we walked further along the thames river. I thought that the Millenium Bridge was the same bridge from the [Challa](https://www.youtube.com/watch?v=9a4izd3Rvdw&themeRefresh=1) song from _Jab Tak Hai Jaan_, (the bridge that Day really wanted to visit) but that bridge was actually the Golden Jubilee Bridge further away, near Westminster.

We walked on Millenium Bridge anyway, and it was fun. After crossing the bridge, we walked to the Shard. That failed miserably.

Mumma thought that since she had the London Pass, we could go and see the view from the Shard. However, the Pass only existed to give you a free ticket, and we had no ticket. Therefore, we were unable to go up to the 52nd floor of the Shard. Instead, we walked back to our hotel.

We were super tired. That night, all the adults went to have drinks. All of us kids, once again, stayed up until they came back.