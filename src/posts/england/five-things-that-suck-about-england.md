---
title: "Five things that suck about England"
date: 2022-08-17 23:00:00
tagged: ["travel", "london2022"]
---

England was a cool place. It was diverse and had lots to do and see. Yet, in many aspects, in sucked compared to the US.

## tl;dr
1. Lack of air conditioning
2. Hard to find cold water
3. Everything closes at 5
4. Coke sucks
5. Hard to find trash cans

## 1. Lack of air conditioning
The major problem I had with the UK was with air conditioning, or lack thereof. 
In 90% of the buildings you go to, there is probably no AC. It gets so hot and 
it was a real pain for Day to find hotels with AC, and the one he didn't book, 
Leicester, had no AC and was a serious pain.

## 2. Hard to find cold water and ice
Getting _cold_ water was hard. In the US water bottle refilling stations are a 
dime a dozen, but in the UK, you were really lucky if you found one. So often 
you would just have to stick to tap water, and British tap water does not taste 
that good.

## 3. Everything closes at 17:00
What is with these odd timings! Just about every business, from a small shop to 
a Pret, closes at five. The British Museum opens at 10:30 and closed at 17:00. 
Buckingham Palace's last ticket was at 17:15. It gets really annoying especially 
when searching for food or for shopping.

Even St. Pauls Cathedral, which we wanted to go on [day 7](/posts/england/day7__tower-london-tower-bridge) closed at 4, so we couldn't go.

## 4. Coke sucks
This really irks me. In the UK, the coke is so mild! Compare it to the US - nice, strong coke. In England, Coke tastes like sugary water.

## 5. Hard to find garbage cans
This got really annoying on the last day. We had breakfast on the go from Pret, 
and had to carry a bag of garbage the entire walk to Sky Garden. Why can't there 
just be more garbage cans in places?